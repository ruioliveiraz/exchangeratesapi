package com.rho.exchangeengine.cache;

import org.springframework.stereotype.Component;
import org.springframework.cache.interceptor.KeyGenerator;

import java.lang.reflect.Method;


@Component("keyGen")
public class MyCustomKeyGenerator implements KeyGenerator {

    public Object generate(Object target, Method method, Object... params) {

        StringBuilder sb = new StringBuilder();

        for (Object param : params) {
            sb.append(param);
        }

        return sb.toString();

    }

}
