package com.rho.exchangeengine.handlers.exceptions;

import com.rho.exchangeengine.auxiliarclasses.responses.ExchangeRateResponse;
import org.springframework.beans.TypeMismatchException;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {


    public RestExceptionHandler() {
        super();
    }

    @Override
    protected ResponseEntity<Object> handleHttpMediaTypeNotSupported(HttpMediaTypeNotSupportedException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        StringBuilder errorMessage = new StringBuilder();
        errorMessage.append(ex.getContentType());
        errorMessage.append(" media type is not supported. Supported media types are ");
        ex.getSupportedMediaTypes().forEach(t -> errorMessage.append(t).append(", "));
        return buildResponseEntity(new ExchangeRateResponse( errorMessage.toString()), HttpStatus.BAD_REQUEST);
    }


    @Override
    protected ResponseEntity<Object> handleMissingServletRequestParameter(MissingServletRequestParameterException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        String errorMessage = String.format("Parameter %s is missing", ex.getParameterName());
        return buildResponseEntity(new ExchangeRateResponse( errorMessage), HttpStatus.BAD_REQUEST);
    }


    @Override
    protected ResponseEntity<Object> handleTypeMismatch(TypeMismatchException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        String errorMessage = String.format("Paramenter could not be converted to type %s", ex.getRequiredType().getSimpleName());
        return buildResponseEntity(new ExchangeRateResponse( errorMessage), HttpStatus.BAD_REQUEST);

    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        String errorMessage = "Malformed JSON request";
        return buildResponseEntity(new ExchangeRateResponse( errorMessage), HttpStatus.BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotWritable(HttpMessageNotWritableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        String errorMessage = "Error handling JSON request";
        return buildResponseEntity(new ExchangeRateResponse( errorMessage), HttpStatus.BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleNoHandlerFoundException(NoHandlerFoundException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        String errorMessage = String.format("Could not find the %s method for URL %s", ex.getHttpMethod(), ex.getRequestURL());
        return buildResponseEntity(new ExchangeRateResponse( errorMessage), HttpStatus.BAD_REQUEST);
    }


    private ResponseEntity<Object> buildResponseEntity(ExchangeRateResponse apiError, HttpStatus httpStatus ) {
        return new ResponseEntity<>(apiError, httpStatus);
    }
}
