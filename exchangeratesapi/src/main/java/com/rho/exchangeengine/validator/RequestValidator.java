package com.rho.exchangeengine.validator;


import com.rho.exchangeengine.auxiliarclasses.responses.ExchangeRateResponse;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

@Component
public class RequestValidator {

    public ExchangeRateResponse validateRequest(Errors errors) {

        ExchangeRateResponse errorMessageApi = new ExchangeRateResponse();
        if (!errors.getAllErrors().isEmpty()) {
            for (ObjectError error_ : errors.getAllErrors()) {
                FieldError fieldError = (FieldError) error_;
                String field = fieldError.getField().substring(0, 1).toUpperCase() + fieldError.getField().substring(1);
                String errorMessage = String.format("%s %s.", field, error_.getDefaultMessage());
                errorMessageApi.addError(errorMessage);
            }
            errorMessageApi.setStatus(HttpStatus.BAD_REQUEST);
        } else {
            errorMessageApi.setStatus(HttpStatus.OK);
        }

        return errorMessageApi;

    }

}
