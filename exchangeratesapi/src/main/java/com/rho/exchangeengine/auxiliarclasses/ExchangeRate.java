package com.rho.exchangeengine.auxiliarclasses;

import java.io.Serializable;
import java.util.Map;

public class ExchangeRate implements Serializable {

    private String base;

    private String date;

    private Map<String, Double> rates;

    private String error;


    public ExchangeRate() {

    }

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public Map<String, Double> getExchangeRates() {
        return this.rates;
    }

    public void setExchangeRates(Map<String, Double> exchangeRates) {
        this.rates = exchangeRates;
    }


    public ExchangeRate getExchangeRate(String baseCurrency) {
        return null;
    }

    public double getRateForCurrency(String currency) {
        try {
            return this.rates.get(currency);
        } catch (NullPointerException e) {
            throw e;
        }
    }

    public double amountConversion(double amount, String currency) {
        Double exchangeRate;
        try {
            exchangeRate = this.rates.get(currency);
        } catch (NullPointerException e) {
            throw e;
        }

        return amount * exchangeRate;

    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Map<String, Double> getRates() {
        return rates;
    }

    public void setRates(Map<String, Double> rates) {
        this.rates = rates;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
