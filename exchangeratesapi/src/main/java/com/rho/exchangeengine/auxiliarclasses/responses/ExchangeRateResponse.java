package com.rho.exchangeengine.auxiliarclasses.responses;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.http.HttpStatus;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ExchangeRateResponse {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String baseCurrency;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String quoteCurrency;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String date;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Double exchangeRate;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Double result;


    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Map<String, Double> exchangeRates;


    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Double amount;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<String> error;

    @JsonIgnore
    private HttpStatus status;


    public ExchangeRateResponse(String baseCurrency, String quoteCurrency, String date) {
        this.baseCurrency = baseCurrency;
        this.quoteCurrency = quoteCurrency;
        this.date = date;
    }

    public ExchangeRateResponse(String baseCurrency, String date, Map<String, Double> exchangeRates) {
        this.baseCurrency = baseCurrency;
        this.date = date;
        this.exchangeRates = exchangeRates;
    }

    public ExchangeRateResponse(HttpStatus status, String error) {
        this.status = status;
        if (this.error == null) {
            this.error = new ArrayList<>();
        }
        this.error.add(error);
    }

    public ExchangeRateResponse(String error) {
        this.status = status;
        if (this.error == null) {
            this.error = new ArrayList<>();
        }
        this.error.add(error);
    }

    public ExchangeRateResponse() {
        this.error = new ArrayList<>();
    }

    public String getBaseCurrency() {
        return baseCurrency;
    }

    public void setBaseCurrency(String baseCurrency) {
        this.baseCurrency = baseCurrency;
    }

    public String getQuoteCurrency() {
        return quoteCurrency;
    }

    public void setQuoteCurrency(String quoteCurrency) {
        this.quoteCurrency = quoteCurrency;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Double getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(Double exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public Map<String, Double> getExchangeRates() {
        return exchangeRates;
    }

    public void setExchangeRates(Map<String, Double> exchangeRates) {
        this.exchangeRates = exchangeRates;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public List<String> getError() {
        return error;
    }

    public void setError(List<String> error) {
        this.error = error;
    }


    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    public void addError(String error) {
        if (this.error != null) {
            this.error.add(error);
        }
    }

    public Double getResult() {
        return result;
    }

    public void setResult(Double result) {
        this.result = result;
    }
}