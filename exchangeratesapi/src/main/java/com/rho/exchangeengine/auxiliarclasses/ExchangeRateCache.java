package com.rho.exchangeengine.auxiliarclasses;

import com.rho.exchangeengine.serializers.ExchangeRateDeserializer;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.ClientProperties;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import javax.ws.rs.ProcessingException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;

@Component
public class ExchangeRateCache {

    public ExchangeRateCache() {
    }

    @Cacheable(cacheNames = "exchangesCache", keyGenerator = "keyGen", sync = true, condition="#date!='latest'")
    public ExchangeRate getExchangeRates(String uri, String baseCurrency, String date) throws IOException, ProcessingException {
        Response response = callExchangeApi(uri);
        if(response.getStatusInfo().getStatusCode() == 200){
            return response.readEntity(ExchangeRate.class);
        }else{
            String resp = response.readEntity(String.class);
            return ExchangeRateDeserializer.deserialize(resp);
        }
    }


    private Response callExchangeApi(String uri) throws ProcessingException {

        ClientConfig configuration = new ClientConfig();
        configuration.property(ClientProperties.CONNECT_TIMEOUT, 1000);
        configuration.property(ClientProperties.READ_TIMEOUT, 1000);
        Client request_ = ClientBuilder.newClient(configuration);

        return request_.target(uri).request(MediaType.APPLICATION_JSON).get();


    }


}
