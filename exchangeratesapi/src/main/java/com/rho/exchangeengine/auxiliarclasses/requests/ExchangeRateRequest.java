package com.rho.exchangeengine.auxiliarclasses.requests;

import javax.validation.constraints.*;
import java.util.List;
import java.util.stream.Collectors;

public class ExchangeRateRequest {

    @NotEmpty
    private String baseCurrency;

    @NotEmpty
    private List<String> quoteCurrencies;

    @NotNull
    @Positive
    private double amount;
    
    private String date;

    public ExchangeRateRequest() {

    }

    public ExchangeRateRequest(String baseCurrency, List<String> quoteCurrencies, double amount) {
        this.baseCurrency = baseCurrency;
        this.quoteCurrencies = quoteCurrencies;
        this.amount = amount;
    }


    public String getBaseCurrency() {
        return baseCurrency;
    }

    public void setBaseCurrency(String baseCurrency) {
        this.baseCurrency = baseCurrency;
    }

    public List<String> getQuoteCurrencies() {

        return this.quoteCurrencies.stream().map(String::toUpperCase).collect(Collectors.toList());

    }

    public void setQuoteCurrencies(List<String> quoteCurrencies) {
        this.quoteCurrencies = quoteCurrencies;
    }


    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

}
