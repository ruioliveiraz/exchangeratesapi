package com.rho.exchangeengine.services;

import com.rho.exchangeengine.auxiliarclasses.requests.ExchangeRateRequest;
import com.rho.exchangeengine.auxiliarclasses.ExchangeRateCache;
import com.rho.exchangeengine.auxiliarclasses.ExchangeRate;
import com.rho.exchangeengine.auxiliarclasses.responses.ExchangeRateResponse;
import org.springframework.stereotype.Service;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

@Service
public class ExchangeRateService {

    public static ExchangeRateResponse getCurrencyExchanges(ExchangeRateCache exchangeRateCache, String baseCurrency, String date) throws IOException {

        StringBuilder uri = new StringBuilder();

        if (date == null || date.equalsIgnoreCase("latest")) {
            date = "latest";
            uri.append("https://api.exchangeratesapi.io/latest?base=").append(baseCurrency);
        } else {
            uri.append("https://api.exchangeratesapi.io/").append(date).append("?base=").append(baseCurrency);
        }

        ExchangeRate exchangeRates = exchangeRateCache.getExchangeRates(uri.toString(), baseCurrency.toUpperCase(), date);

        if (exchangeRates.getError() == null) {
            return new ExchangeRateResponse(exchangeRates.getBase(), exchangeRates.getDate(), exchangeRates.getExchangeRates());
        } else {
            String errorMessage = exchangeRates.getError();
            if (errorMessage.equals("Invalid base")) errorMessage = "Invalid Base Currency";
            else if (errorMessage.equals("Not Found")) errorMessage = "Invalid Date";

            return new ExchangeRateResponse(errorMessage);
        }
    }

    public static ExchangeRateResponse convertCurrencies(ExchangeRateCache exchangeRateCache, ExchangeRateRequest request) throws IOException {

        String baseCurrency = request.getBaseCurrency();
        String date = request.getDate();
        StringBuilder uri = new StringBuilder();

        if (date == null || date.equalsIgnoreCase("latest")) {
            date = "latest";
            uri.append("https://api.exchangeratesapi.io/latest?base=").append(baseCurrency);
        } else {
            uri.append("https://api.exchangeratesapi.io/").append(date).append("?base=").append(baseCurrency);
        }

        ExchangeRate exchangeRates = exchangeRateCache.getExchangeRates(uri.toString(), baseCurrency.toUpperCase(), date);

        Map<String, Double> exchangeRatesCopy = new HashMap<>();

        if (exchangeRates.getError() == null) {

            Map<String, Double> exchangeRates_ = exchangeRates.getExchangeRates();
            exchangeRatesCopy.putAll(exchangeRates_);

            Double value = request.getAmount();
            Iterator<String> it = exchangeRatesCopy.keySet().iterator();

            while (it.hasNext()) {

                String key = it.next().toUpperCase();

                if (!request.getQuoteCurrencies().contains(key)) {

                    it.remove();

                } else {

                    Double convertedValue = exchangeRatesCopy.get(key) * value;
                    exchangeRatesCopy.put(key, convertedValue);

                }

            }

            return new ExchangeRateResponse(exchangeRates.getBase(), exchangeRates.getDate(), exchangeRatesCopy);


        } else {

            String errorMessage = exchangeRates.getError();
            if (errorMessage.equals("Invalid base")) errorMessage = "Invalid Base Currency";
            else if (errorMessage.equals("Not found")) errorMessage = "Invalid Date";

            return new ExchangeRateResponse(errorMessage);

        }

    }

    public static ExchangeRateResponse convertCurrency(ExchangeRateCache exchangeRateCache, String baseCurrency, String quoteCurrency, Double amount, String date) throws IOException {

        StringBuilder uri = new StringBuilder();

        if (date == null || date.equalsIgnoreCase("latest")) {
            date = "latest";
            uri.append("https://api.exchangeratesapi.io/latest?base=").append(baseCurrency);
        } else {
            uri.append("https://api.exchangeratesapi.io/").append(date).append("?base=").append(baseCurrency);
        }

        ExchangeRate exchangeRates = exchangeRateCache.getExchangeRates(uri.toString(), baseCurrency.toUpperCase(), date);

        if (exchangeRates.getError() == null) {

            double finalAmount;

            try {
                finalAmount = exchangeRates.amountConversion(amount, quoteCurrency.toUpperCase());

            } catch (NullPointerException e) {

                String errorMessage = "Quote Currency does not exist.";
                return new ExchangeRateResponse(errorMessage);

            }

            ExchangeRateResponse result =  new ExchangeRateResponse(exchangeRates.getBase(), quoteCurrency.toUpperCase(), exchangeRates.getDate());
            result.setResult(finalAmount);

            return result;

        } else {

            String errorMessage = exchangeRates.getError();
            if (errorMessage.equals("Invalid base")) {
                errorMessage = "Invalid Base Currency";
            } else if (errorMessage.equals("Not found")) {
                errorMessage = "Invalid Date";
            }
            return new ExchangeRateResponse(errorMessage);


        }

    }

    public static ExchangeRateResponse getExchangeFromCurrencyToCurrency(String baseCurrency, String quoteCurrency, ExchangeRateCache exchangeRateCache, String date) throws IOException {

        StringBuilder uri = new StringBuilder();

        if (date == null || date.equalsIgnoreCase("latest")) {
            date = "latest";
            uri.append("https://api.exchangeratesapi.io/latest?base=").append(baseCurrency);
        } else {
            uri.append("https://api.exchangeratesapi.io/").append(date).append("?base=").append(baseCurrency);
        }

        ExchangeRate exchangeRates = exchangeRateCache.getExchangeRates(uri.toString(), baseCurrency.toUpperCase(), date);

        if (exchangeRates.getError() == null) {

            double exchangeRate;

            try {

                exchangeRate = exchangeRates.getRateForCurrency(quoteCurrency.toUpperCase());

            } catch (NullPointerException e) {

                String errorMessage = "Quote Currency does not exist.";
                return new ExchangeRateResponse(errorMessage);

            }

            ExchangeRateResponse result = new ExchangeRateResponse(exchangeRates.getBase(), quoteCurrency.toUpperCase(), exchangeRates.getDate());

            result.setExchangeRate(exchangeRate);

            return result;

        } else {

            String errorMessage = exchangeRates.getError();
            if (errorMessage.equals("Invalid base")) {
                errorMessage = "Invalid Base Currency";
            } else if (errorMessage.equals("Not found")) {
                errorMessage = "Invalid Date";
            }
            return new ExchangeRateResponse(errorMessage);

        }

    }

}
