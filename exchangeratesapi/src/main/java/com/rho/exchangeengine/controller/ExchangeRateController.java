package com.rho.exchangeengine.controller;

import com.rho.exchangeengine.auxiliarclasses.requests.ExchangeRateRequest;
import com.rho.exchangeengine.validator.RequestValidator;
import com.rho.exchangeengine.auxiliarclasses.ExchangeRateCache;
import com.rho.exchangeengine.auxiliarclasses.responses.ExchangeRateResponse;
import com.rho.exchangeengine.services.ExchangeRateService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import javax.validation.constraints.Positive;
import java.io.IOException;

@RestController
@RequestMapping("/exchangeratesapi")
@Api(value = "Exchange rates API", description = "REST API to calculate exchange rates and convert currencies", tags = {"Exchange Rates API"}, produces = MediaType.APPLICATION_JSON_VALUE)
@ControllerAdvice
public class ExchangeRateController {


    private final ExchangeRateCache exchangeRateCache;

    private final RequestValidator requestValidator;

    @Autowired
    public ExchangeRateController(ExchangeRateCache exchangeRateCache, RequestValidator requestValidator) {
        this.exchangeRateCache = exchangeRateCache;
        this.requestValidator = requestValidator;
    }

    @RequestMapping(value = "/getexchangerate", method = RequestMethod.GET)
    @ApiOperation("Gets the exchange rate from one currency to another.")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "OK", response = ResponseEntity.class),
            @ApiResponse(code = 400, message = "Invalid base currency || Invalid Date || Date to Old || Missing parameters || Type mismatch etc", response = ResponseEntity.class),  @ApiResponse(code = 500, message = "Internal server error", response = ResponseEntity.class)})
    public ResponseEntity getExchangeFromCurrencyToCurrency(@RequestParam(value = "baseCurrency") @ApiParam(value = "The currency to which exchange rates are quoted. The currency identifier do not need to be in capital letters. ", required = true, defaultValue = "USD") String baseCurrency,
                                                            @RequestParam(value = "quoteCurrency") @ApiParam(value = "The currency to which exchange rates are requested. The currency identifier do not need to be in capital letters.", required = true, defaultValue = "EUR") String quoteCurrency,
                                                            @RequestParam(value = "date", required = false) @ApiParam(value = "Date in which the exchange rate data was collected. If the user wants the lastest available exchange rate, they can omit the Date or even insert \"latest\". If the date is greater than the last available date, the latest available data is considered.", defaultValue = "latest") String date) {

        if (baseCurrency.compareToIgnoreCase(quoteCurrency) == 0) {
            String errorMessage = "Quote Currency and Base Currency must be different";
            return new ResponseEntity<>(new ExchangeRateResponse(errorMessage), HttpStatus.BAD_REQUEST);
        }

        ExchangeRateResponse response;
        try {
            response = ExchangeRateService.getExchangeFromCurrencyToCurrency(baseCurrency, quoteCurrency, this.exchangeRateCache, date);
        } catch (IOException e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        if (response.getError() == null) {
            return new ResponseEntity<>(response, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }

    }

    @ApiOperation("Gets all exchange rates for a specific currency.")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "OK", response = ResponseEntity.class),@ApiResponse(code = 400, message = "Invalid base currency || Invalid Date || Date to Old || Missing parameters || Type mismatch etc", response = ResponseEntity.class),  @ApiResponse(code = 500, message = "Internal server error", response = ResponseEntity.class)})
    @RequestMapping(value = "/getexchangerates", method = RequestMethod.GET)
    public ResponseEntity getCurrencyExchanges(@RequestParam(value = "baseCurrency") @ApiParam(value = "The currency to which exchange rates are quoted. The currency identifier do not need to be in capital letters.", required = true, defaultValue = "USD") String baseCurrency,
                                               @RequestParam(value = "date", required = false) @ApiParam(value = "Date in which the exchange rate data was collected. If the user wants the lastest available exchange rate, they can omit the Date or even insert \"latest\". If the date is greater than the last available date, the latest available date is considered.", defaultValue = "2018-10-05") String date) {


        ExchangeRateResponse response;
        try {
            response = ExchangeRateService.getCurrencyExchanges(this.exchangeRateCache, baseCurrency, date);
        } catch (IOException e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        if (response.getError() == null) {
            return new ResponseEntity<>(response, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }

    }

    @ApiOperation("Converts any amount from one currency to another.")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "OK", response = ResponseEntity.class),
            @ApiResponse(code = 400, message = "Invalid base currency || Invalid Date || Date to Old || Missing parameters || Type mismatch etc", response = ResponseEntity.class),  @ApiResponse(code = 500, message = "Internal server error", response = ResponseEntity.class)})
    @RequestMapping(value = "/converttocurrency", method = RequestMethod.GET)
    public ResponseEntity convertCurrency(@Valid @RequestParam(value = "baseCurrency") @ApiParam(value = "The currency to which exchange rates are quoted. The currency identifier do not need to be in capital letters.", required = true, defaultValue = "USD") String baseCurrency,
                                          @RequestParam(value = "quoteCurrency")  @ApiParam(value = "The currency an amount is converted to. The currency identifier do not need to be in capital letters.", required = true, defaultValue = "EUR") String quoteCurrency,
                                          @RequestParam(value = "amount")  @Positive @ApiParam(value = " Amount - value that is intended to be converted.", required = true, defaultValue = "20") double amount,
                                          @RequestParam(value = "date", required = false) @ApiParam(value = "Date in which the exchange rate data was collected. If the user wants the lastest available exchange rate, they can omit the Date or even insert \"latest\". If the date is greater than the last available date, the latest available date is considered.", defaultValue = "2010-10-10") String date) {


        if(amount <= 0){
            String errorMessage = "Amount must be greater than 0.";
            return new ResponseEntity<>(new ExchangeRateResponse(errorMessage), HttpStatus.BAD_REQUEST);
        }

        if (baseCurrency.compareToIgnoreCase(quoteCurrency) == 0) {
            String errorMessage = "Quote Currency and Base Currency must be different";
            return new ResponseEntity<>(new ExchangeRateResponse(errorMessage), HttpStatus.BAD_REQUEST);
        }

        ExchangeRateResponse response;

        try {
            response = ExchangeRateService.convertCurrency(this.exchangeRateCache, baseCurrency, quoteCurrency, amount, date);
        } catch (IOException e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        if (response.getError() == null) {
            return new ResponseEntity<>(response, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }

    }


    @ApiOperation(value = "Converts any amount from one currency to a list of supplied currencies.")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "OK", response = ResponseEntity.class), @ApiResponse(code = 400, message = "Invalid base currency || Invalid Date || Date to Old || Missing parameters || Type mismatch etc", response = ResponseEntity.class), @ApiResponse(code = 500, message = "Internal server error", response = ResponseEntity.class)})
    @PostMapping(value = "/converttocurrencies", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity convertCurrencies(@Valid @RequestBody @ApiParam(
            value = "The request body must be composed by: \n Base currency - The currency to which exchange rates are quoted. The currency identifier do not need to be in capital letters. \n Quote currencies - List of currencies that the amount is converted to. The currency do not need to be in capital letters. \n Amount - value that is intended to be converted. \n Date - Date in which the exchange rate data was collected. If the user wants the lastest available exchange rate, they can omit the Date or even insert \"latest\". If the date is greater than the last available date, the latest available date is considered. \n If none of the quote currencies exist in the list will be empty.", required = true
    ) ExchangeRateRequest body, BindingResult errors) {

        ExchangeRateResponse response;

        response = this.requestValidator.validateRequest(errors);
        if (!response.getStatus().equals(HttpStatus.OK)) {
            return new ResponseEntity<>(response, response.getStatus());
        } else {
            try {
                response = ExchangeRateService.convertCurrencies(this.exchangeRateCache, body);
            } catch (IOException e) {
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
            if (response.getError() == null) {
                return new ResponseEntity<>(response, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }

        }


    }

}
