package com.rho.exchangeengine.serializers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rho.exchangeengine.auxiliarclasses.ExchangeRate;
import java.io.IOException;

public class ExchangeRateDeserializer {

    public static ExchangeRate deserialize(String x) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(x, ExchangeRate.class);
    }
}
