# Exchange Engine challenge 

## Usage

First, to start the application run the following commands in the project folder:

```

$ gradle build
$ ./gradlew run
```

## Documentation

After the application has been started the documentation and the testing interface are available at:

```
http://localhost:8080/swagger-ui.html#/ExchangeEngineChallenge
```

## Implementation details

### Cache

* The cache implementation used in this project is ehcache - http://www.ehcache.org.
* The access to the cache was made synchronous due to distributed systems requirements.
* In the cache are saved the values retrieved from the external API even if it returns an error. Saving the error message returned by the API it is possible to reduce the requests to the external API with invalid parameters (invalid baseCurrency or date).
* The key used to retrieve/save values from/to the cache is the concatenation of the base currency identifier and the date.
* Whenever a request wants to retrieve the lastest exchange rates for a currency, the information that has been retrieved from the external API is not saved. This approach is needed to prevent that the lastest exchange rates in cache does not correspond to lastest exchange rates available in the external API. 

## Error Handling
On all requests if an error occurs it is returned a JSON with the following format: 

```
{
	'error': 'error'
}
```

The error filed is a hint to understand the error so it varies according to the source of the error. Some of them are:

* Missing parameter;
* Invalid date;
* Invalid baseCurrency;
* Invalid baseCurrencies;
* Error handling JSON request;
* Malformed JSON request;
* Media type is not supported.





